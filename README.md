# lambda-flask

This is an sample application with Python Flask as AWS Lambda Function URL

## Versions

- Python: 3.9.6
  - Note that Supported Lambda runtime is Python 3.9
  - each package versions for Python is described in `requirements.txt`
- aws-cli

## Build local environment

Currently this sample app would expect run with OSX, and supporting local database is `SQLite3`

```bash
# Create virtualenv for splitting current env
$ python -m venv venv
$ source venv/bin/activate

# Install deps
$ pip install -r requirements.txt

# Start application
$ python lambda_function.py
 * Serving Flask app 'lambda_function'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:8000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 487-010-513
```

## Starting applications

As source codes are implemented using Flask frameworks, for starting application, you can use `flask` commands, which would be installed when you intall pip package of flask.
Consider the hander path would be different from default lambda ones, `lambda_function`, so we need to change `api.lambda_function` when running `flask` command.

```bash
# Start applications with flask-cli
% FLASK_APP='api.lambda_function' flask run
 * Serving Flask app 'api.lambda_function'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit

# On another window or tabs, you can easily call APIs for application
% curl -s -X GET localhost:5000/healthz |jq .
{
  "status": "ok"
}
```

## Setup local PostgreSQL

In case you have docker environment on your laptop, you can have a choice for local database with `PostgreSQL` container.
We have already had configurations to build up PostgreSQL container with docker-compose in `compose.yml` files.

Note that if you have access to PostgreSQL with Python, it required to install additional packages `pyscopg2`.
For deployment environment (AWS Lambda), the package is already listed in `requirements.txt`, but for your local environement,
you need to allow to install drivers onto your OS, **since `pycopy2` pakcages would have dependencies with runtime-level for C libraries.**

```bash
# Start PostgreSQL container with docker-compose (by default `compose.yml` would be loaded)
$ docker compose up --build --detach
$ docker compose ps

# If you want to have direct access to PostgreSQL:
$ docker exec -it postgres /bin/bash
root@47230f5912b7:/#
root@47230f5912b7:/# psql -h localhost -U postgres
```

## Unit Testings

Since we can use `pytest` for unit testings for lambda application codes, you can easily run tests with using the libraries and its CLI.
Note that databases for application sources and testings applications are separated, with using Flask blueprint registrations.

```bash
# Change virtualenv if needed, and check path of pytest CLI
$ source venv/bin/activate
$ which pytest

# Run Testings
% pytest -s -vvv ./tests
========================================================================================= test session starts ==========================================================================================
platform darwin -- Python 3.9.6, pytest-7.2.2, pluggy-1.0.0 -- /Users/hwakabayashi/git/lambda-flask/.venv/bin/python
cachedir: .pytest_cache
rootdir: /Users/hwakabayashi/git/lambda-flask
collected 5 items

tests/test_api_base.py::test_api_root PASSED
tests/test_api_base.py::test_api_healthz PASSED
tests/test_users.py::test_user_create PASSED
tests/test_users.py::test_user_list PASSED
tests/test_users.py::test_user_read PASSED

========================================================================================== 5 passed in 0.05s ===========================================================================================
```

## Local functions testings

For local testings running as Lambda Functions, we could use `lambci/lambda` container images.
With using lambci container image, the events and responses in AWS Lambda would be emurated in your local environment.
See more information to find more options for your local testings, please refer to the [official documents of lambci](https://hub.docker.com/r/lambci/lambda).

```bash
# Pulling images for testings
$ docker image pull lambci/lambda:python3.8

# Run oneoff calls (Invocations) with your application source
% docker run --rm -v "$PWD:/var/task:ro,delegated" -v "$PWD:/opt:ro,delegated" lambci/lambda:python3.8 api.lambda_function.lambda_handler $(cat event.local.json |jq -c)
START RequestId: 12ba437e-58ab-16fc-b94d-56eea03ff7f0 Version: $LATEST
END RequestId: 12ba437e-58ab-16fc-b94d-56eea03ff7f0
REPORT RequestId: 12ba437e-58ab-16fc-b94d-56eea03ff7f0	Init Duration: 236.01 ms	Duration: 12.09 ms	Billed Duration: 13 ms	Memory Size: 1536 MB	Max Memory Used: 38 MB

# Response as Lambda Function URL below
# (...)
```
