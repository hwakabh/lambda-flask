import os
from pathlib import Path

import awsgi

from . import create_app
from .models import db
from .routes.base import base
from .routes.users import users

# Setup application
app = create_app()
# Add application routings for blueprints
app.register_blueprint(base)
app.register_blueprint(users)

# Register db for app
BASE_DIR = Path(__file__).resolve().parent
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DB_URL", f"sqlite:///{Path(BASE_DIR).joinpath('../sqlite3.db')}")

db.init_app(app=app)
with app.app_context():
    db.create_all()


# For handling request with Lambda
def lambda_handler(event,context):
    # Since aws-wsgi does not support API Payload version 2.0, need to modify event object
    # https://github.com/slank/awsgi/pull/58
    event['httpMethod'] = event['requestContext']['http']['method']
    event['path'] = event['requestContext']['http']['path']

    return awsgi.response(app, event, context)


if __name__ == "__main__":
    app.run(port=8000, debug=True)
