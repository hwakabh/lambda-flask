from flask import Flask


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    # https://flask.palletsprojects.com/en/2.3.x/tutorial/factory/
    # define common API routes to factory if needed
    # @app.route(...)

    return app
