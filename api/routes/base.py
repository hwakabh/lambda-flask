from flask import (
    Blueprint,
    jsonify,
)


base = Blueprint("base_api", __name__)

@base.route("/", methods=["GET"])
def root():
    return jsonify(status='ok', message='Hello world from Flask-Lambda')


@base.route("/healthz", methods=["GET"])
def healthz():
    return jsonify(status="ok")
