from flask import (
    Blueprint,
    request,
    jsonify,
)

from api.models import db
from api.models.users import User


users = Blueprint("users_api", __name__)

@users.route("/users", methods=["GET", "POST"])
def crud_users():
    try:
        if request.method == 'GET':
            users = db.session.query(User).all()
            resp = [user.as_dict() for user in users]

            return jsonify(status='OK', data=resp)

        if request.method == 'POST':
            req = request.get_json()

            # request body lazy validation
            if len(req.keys()) != 1:
                return jsonify(error='Only one data is acceptable')
            if 'name' not in req.keys():
                return jsonify(error='key:name is required')

            user = User(name=req.get('name'))
            db.session.add(user)
            db.session.commit()

            return jsonify(message="OK", data=request.get_json())

    except Exception as e:
        return jsonify(error=str(e))


@users.route("/users/<int:id>", methods=["GET", "PATCH", "DELETE"])
def crud_each_user(id):
    try:
        # user = session.query(User).get(id)
        user = db.session.get(User, id)
    except Exception as e:
        return jsonify(error=str(e))

    if request.method == 'GET':
        return jsonify(message='OK', data=user.as_dict())

    if request.method == 'PATCH':
        # lazy-validation
        req = request.get_json()
        if not req.get('name', None):
            return jsonify(message="Failed to update", error="PATCH accept only key:name")
        else:
            user.name = req.get('name')
            db.session.commit()
            return jsonify(message='ok-patched', data=user.as_dict())


    if request.method == 'DELETE':
        db.session.delete(user)
        db.session.commit()
        try:
            return jsonify(message="ok-deleted")
        except Exception as e:
            return jsonify(message="Failed to delete", error=str(e))
