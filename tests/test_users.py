def test_user_create(appclient):
      resp = appclient.post('/users', json={"name": "testdata-alice"})
      assert resp.status_code == 200

      assert len(resp.json['data']) == 1
      assert resp.json['message'] == 'OK'


def test_user_list(appclient):
      resp = appclient.get('/users')
      assert resp.status_code == 200

      assert resp.json['status'] == 'OK'
      assert len(resp.json['data']) == 1


def test_user_read(appclient):
      resp = appclient.get('/users')
      assert resp.status_code == 200

      assert resp.json['status'] == 'OK'
