def test_api_root(baseapp):
      resp = baseapp.get('/')
      assert resp.status_code == 200


def test_api_healthz(baseapp):
      resp = baseapp.get('/healthz')
      assert resp.status_code == 200
      assert resp.json['status'] == 'ok'

