from pathlib import Path
import os
import tempfile

import pytest

from api import create_app
from api.models import db
from api.routes.base import base
from api.routes.users import users


# Case if testings do not required any database connections
@pytest.fixture(scope='function')
def baseapp():
      app = create_app({
              'TESTING': True,
      })
      app.register_blueprint(base)
      app.register_blueprint(users)

      yield app.test_client()


# Case if testings required DB connections
@pytest.fixture(scope='function')
def appclient():
    if os.environ.get('DB_HOST', None):
        # # TODO: change SQLAlchemy dialect to PostgreSQL by docker-compose
        # print('Using local DB: PostgreSQL')

        BASE_DIR = Path(__file__).resolve().parent.parent

        app = create_app({
                'TESTING': True,
                'DATABASE': BASE_DIR.joinpath("testings-postgresql.db"),
            })
        app.register_blueprint(users)
        app.config['SQLALCHEMY_DATABASE_URI'] = f"sqlite:///{Path(BASE_DIR).joinpath('testings-postgresql.db')}"

        db.init_app(app)
        with app.app_context():
            db.create_all()

        yield app.test_client()

    else:
        db_fd, db_path = tempfile.mkstemp()
        # print(f'Using local DB: SQLite3: [ {db_path} ]')

        app = create_app({
                'TESTING': True,
                'DATABASE': f'{db_path}',
            })
        app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{db_path}'
        app.register_blueprint(users)

        db.init_app(app)
        with app.app_context():
            db.create_all()

            # TODO: load intial SQLs from files
            # INSERT inital data to testing database
            from api.models.users import User
            user = User(name='initial-data-01')
            db.session.add(user)
            db.session.commit()

        yield app.test_client()

        # Teardown
        with app.app_context():
            db.drop_all()
        os.close(db_fd)
        os.unlink(db_path)
